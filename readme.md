# c-argh

## why

minimal [argument parsing](https://github.com/nathants/c-argh/blob/master/example.c) shouldn't require a hundred lines of code.

## what

header only argument parsing for c inspired by the simplicity of [argh](https://pythonhosted.org/argh/).

## example

```bash
>> make

>> ./example.out -lph5 asdf 123
head: 5, prefix: 1, lz4: 1
pos arg 0: asdf
pos arg 1: 123

>> ./example.out --lz4 asdf -p 123 --head 5
head: 5, prefix: 1, lz4: 1
pos arg 0: asdf
pos arg 1: 123

>> ./example.out asdf 123 --head 5 --lz4
head: 5, prefix: 0, lz4: 1
pos arg 0: asdf
pos arg 1: 123
```
